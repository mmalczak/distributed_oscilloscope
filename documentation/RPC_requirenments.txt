XML_RPC drwabacks:
- not able to sne dthe dictionaries with integer keys
- not able to send integers bigger than 24 bits
- not able to send notifications
- not able to send data without response
- not able to establish just one connection - every request is seperate 
    connection


gRPC drawbacks:
- difficult/impossible monitoring of the state of the channel
- no direct way to manually poll on the connection - we don't want unnecessary 
    threads, we want one thread to poll on both ADC file descriptor and the 
    connection
- more complicated with little added value - at this point - certainly it 
    would have many cons if the performance was  taken into account

ZeroMQ drawbacks:
- not possible to monitor the connection, heartbeating necessary
- REQ-REP pattern will be deprecated, the recommended pattern is 
    client-server, which is not supported in python bindings



requierements:
- monitor the connection
- possibility of notifications, sync and async messages
- poll on file descriptor, to avoid threading
